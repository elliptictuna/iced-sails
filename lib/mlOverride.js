module.exports = function (sails, moduleloader) {
  
  /**
   * Module dependencies
   */

  var buildDictionary = require('sails-build-dictionary');
  var async = require('async');
  var path = require('path');

  /**
   * Module loader
   *
   * Load a module into memory
   */
  return {
    // Default configuration
    configure: function () {
      // Enable server-side IcedCoffeeScript support
      try {
        require('iced-coffee-script/register');
      } catch(e0){
        try {
          var appPath = sails.config.appPath || process.cwd();
          require(path.join(appPath, 'node_modules/iced-coffee-script/register'));
        }
        catch (e1) {
          sails.log.verbose('Please run `npm install iced-coffee-script` to use icedcoffescript (skipping for now)');
          sails.log.silly('Here\'s the require error(s): ',e0,e1);
        }
      }
    },

   /**
     * Load user config from app
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadUserConfig: function (cb) {

      async.auto({

        'config/*': function loadOtherConfigFiles (cb) {
          buildDictionary.aggregate({
            dirname   : sails.config.paths.config || sails.config.appPath + '/config',
            exclude   : [
              'locales',
              'local.js',
              'local.json',
              'local.coffee',
              'local.coffee.md',
              'local.litcoffee',
              'local.iced',
              'local.iced.md',
              'local.liticed',
              ],
            excludeDirs: /(locales|env)$/,
            filter    : /(.+)\.(js|json|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
            identity  : false
          }, cb);
        },


        'config/local' : function loadLocalOverrideFile (cb) {
          buildDictionary.aggregate({
            dirname   : sails.config.paths.config || sails.config.appPath + '/config',
            filter    : /local\.(js|json|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
            identity  : false
          }, cb);
        },


        'config/env/*' : ['config/local', function loadLocalOverrideFile (cb, async_data) {
          // If there's an environment already set in sails.config, then it came from the environment
          // or the command line, so that takes precedence.  Otherwise, check the config/local.js file
          // for an environment setting.  Lastly, default to development.
          var env = sails.config.environment || async_data['config/local'].environment || 'development';
          buildDictionary.aggregate({
            dirname   : (sails.config.paths.config || sails.config.appPath + '/config') + '/env',
            filter    : new RegExp(env + '.(js|json|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$'),
            optional  : true,
            identity  : false
          }, cb);
        }]

      }, function (err, async_data) {
        if (err) return cb(err);
        // Save the environment override, if any.
        var env = sails.config.environment;
        // Merge the configs, with local.js taking precedence over others, and env/*.js files
        // taking precedence over local.js
        var config = sails.util.merge(
          async_data['config/*'],
          async_data['config/local'],
          async_data['config/env/*']
        );
        // Set the environment.
        config.environment = env || config.environment || 'development';
        // Return the user config
        cb(null, config);
      });
    },



    /**
     * Load app controllers
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadControllers: function (cb) {
      buildDictionary.optional({
        dirname: sails.config.paths.controllers,
        filter: /(.+)Controller\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        flattenDirectories: true,
        keepDirectoryPath: true,
        replaceExpr: /Controller/
      }, cb);
    },




    /**
     * Load adapters
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadAdapters: function (cb) {
      buildDictionary.optional({
        dirname   : sails.config.paths.adapters,
        filter    : /(.+Adapter)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        replaceExpr : /Adapter/,
        flattenDirectories: true
      }, cb);
    },




    /**
     * Load app's model definitions
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadModels: function (cb) {
      // Get the main model files
      buildDictionary.optional({
        dirname   : sails.config.paths.models,
        filter    : /^([^.]+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        replaceExpr : /^.*\//,
        flattenDirectories: true
      }, function(err, models) {
        if (err) {return cb(err);}
        // Get any supplemental files
        buildDictionary.optional({
          dirname   : sails.config.paths.models,
          filter    : /(.+)\.attributes.json$/,
          replaceExpr : /^.*\//,
          flattenDirectories: true
        }, function(err, supplements) {
          if (err) {return cb(err);}
          return cb(null, sails.util.merge(models, supplements));
        });
      });
    },





    /**
     * Load app services
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadServices: function (cb) {
      buildDictionary.optional({
        dirname     : sails.config.paths.services,
        filter      : /(.+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        depth     : 1,
        caseSensitive : true
      }, cb);
    },


    /**
     * Load app policies
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadPolicies: function (cb) {
      buildDictionary.optional({
        dirname: sails.config.paths.policies,
        filter: /(.+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        replaceExpr: null,
        flattenDirectories: true,
        keepDirectoryPath: true
      }, cb);
    },



    /**
     * Load app hooks
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadUserHooks: function (cb) {
      buildDictionary.optional({
        dirname: sails.config.paths.hooks,
        filter: /^(.+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,

        // Hooks should be defined as either single files as a function
        // OR (better yet) a subfolder with an index.js file
        // (like a standard node module)
        depth: 2
      }, cb);
    },



    /**
     * Load app blueprint middleware.
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadBlueprints: function (cb) {
      buildDictionary.optional({
        dirname: sails.config.paths.blueprints,
        filter: /(.+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        useGlobalIdForKeyName: true
      }, cb);
    },



    /**
     * Load custom API responses.
     *
     * @param {Object} options
     * @param {Function} cb
     */
    loadResponses: function (cb) {
      buildDictionary.optional({
        dirname: sails.config.paths.responses,
        filter: /(.+)\.(js|coffee|coffee.md|litcoffee|iced|iced.md|liticed)$/,
        useGlobalIdForKeyName: true
      }, cb);
    },

  };
};
